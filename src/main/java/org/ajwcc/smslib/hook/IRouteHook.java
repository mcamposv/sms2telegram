
package org.ajwcc.smslib.hook;

import java.util.Collection;
import org.ajwcc.smslib.gateway.AbstractGateway;
import org.ajwcc.smslib.message.OutboundMessage;

public interface IRouteHook
{
	public Collection<AbstractGateway> process(OutboundMessage message, Collection<AbstractGateway> gateways);
}
