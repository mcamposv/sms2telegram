
package org.ajwcc.smslib.hook;

import org.ajwcc.smslib.message.OutboundMessage;

public interface IPreQueueHook
{
	public boolean process(OutboundMessage message);
}
