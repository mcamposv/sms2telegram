
package org.ajwcc.smslib.routing;

import java.util.Collection;
import org.ajwcc.smslib.gateway.AbstractGateway;
import org.ajwcc.smslib.message.OutboundMessage;

public class DefaultRouter extends AbstractRouter
{
	@Override
	public Collection<AbstractGateway> customRoute(OutboundMessage message, Collection<AbstractGateway> gateways)
	{
		return gateways;
	}
}
