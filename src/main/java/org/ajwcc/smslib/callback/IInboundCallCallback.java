
package org.ajwcc.smslib.callback;

import org.ajwcc.smslib.callback.events.InboundCallCallbackEvent;

public interface IInboundCallCallback
{
	public boolean process(InboundCallCallbackEvent event);
}
