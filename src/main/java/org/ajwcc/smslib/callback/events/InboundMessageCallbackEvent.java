
package org.ajwcc.smslib.callback.events;

import org.ajwcc.smslib.message.InboundMessage;

public class InboundMessageCallbackEvent extends BaseCallbackEvent
{
	InboundMessage message;

	public InboundMessageCallbackEvent(InboundMessage message)
	{
		this.message = message;
	}

	public InboundMessage getMessage()
	{
		return this.message;
	}
}
