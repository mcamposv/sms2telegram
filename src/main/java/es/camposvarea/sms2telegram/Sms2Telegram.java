package es.camposvarea.sms2telegram;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fazecast.jSerialComm.*;
import es.camposvarea.utils.Cfg;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Instant;
import java.util.List;
import java.util.Scanner;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.configuration2.*;
import org.apache.commons.configuration2.builder.fluent.Configurations;


/**
 *
 * @author guybrush
 */
public class Sms2Telegram {
    static Boolean debug=true;
    static Logger LOG;
    static Cfg cfg;
    static Dbmanager database;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, Exception {
        // TODO code application logic here
        try {
            System.setProperty("log4j.configurationFile","cfg/log4j2.json");
            LOG = LogManager.getLogger("MainLog");
            //Load Configuration File.
            cfg = new Cfg();
            System.out.println("Vamos al lio");
            System.out.println("Dumping Serial Ports Config");
            System.out.println("Device:"+cfg.GetCfgvalue("ModemDevice"));
            System.out.println("dbfile:"+cfg.GetCfgvalue("dbfile"));
            database = new Dbmanager(cfg.GetCfgvalue("dbfile"));
            Telegram telegram = new Telegram(cfg.GetCfgvalue("TelegramRoomID"),cfg.GetCfgvalue("TelegramBootApiKey"));
            
            //Retrying failed Elements;
            if(debug) System.out.println("Retrying Failed Messages");
            List<Sms> failed= database.getFailedSms();
            failed.forEach ((sms) ->{
                    try{ // Update to DB
                        try{ //Send to telegram
                            telegram.SendSMS(sms);
                            sms.sent();
                        }
                        catch(Exception e){
                            sms.fail();
                        }// send to Telegram
                        database.writetoSmS(sms);
                    }
                    catch (Exception e) {
                        //Failure to Write to DB do not delete from card.
                        System.out.println("ERROR: Updating to DB");
                    }//Write to Database
            });
            
            zte modem = new zte(cfg.GetCfgvalue("ModemDevice"));
            if (modem.smsInMem()){
                List<Integer> poss = modem.getsmspossitions();
                if(debug) System.out.println("Getting New SMS");
                for (int i=0;i<poss.size();i++){
                    if(debug) System.out.println("Posicion: "+String.valueOf(poss.get(i)));
                    if(debug) System.out.println("El mensaje:"+modem.getsms(Integer.valueOf(i+1)));
                    Sms sms = new Sms(modem.getsmsraw(Integer.valueOf(i+1)));
                    if (debug) System.out.println(sms.toString());
                    try{ // Write to DB
                        try{ //Send to telegram
                            telegram.SendSMS(sms);
                            sms.sent();
                        }
                        catch(Exception e){
                            sms.fail();
                        }// send to Telegram
                        database.writetoSmS(sms);
                        modem.deleteSms(Integer.valueOf(i+1));
                    }
                    catch (Exception e) {
                        //Failure to Write to DB do not delete from card.
                        System.out.println("ERROR: Writing to DB");
                    }//Write to Database
                }
            };
        if (debug) System.out.println("Exiting Succesfully");
        if (debug) System.out.println("Last Run: "+Instant.now().toString());
        } //Global try
        catch(Exception e) {
            LOG.error("Me he calzado una ostia GLOBAL");
            System.out.println(e);
        }//catch global
    }
    
}
