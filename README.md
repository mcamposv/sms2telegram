
# sms2Telegram

This Application will forward SMS to Telegram Group.
I will need a 3G modem to read messages from the simcard
Simcard must be password less configuration.
you wil also have to have configured a Telegram bot and a group where the bot is included to recive the messages.

At The moment The application is lacking a lot of funcionalities but it includes the basics.
The application is prepared to be executed from a crontab read all messages and fw them to telegram and store in a sqllite database.

## Todo Things
Error handling  
Daemonize the process not to be executed from crontab.  
Better login Clean debug messages.  


## Configuration
configuration file:  
cfg/sms2telegram.properties  
File for loggin setup (at the moment there is a lot of debuggin stuf on the stdout.  
cfg/log4j2.json  


## Execution sample
cd < Applcation install path >   
export JAVA_HOME=/opt/java/java  
$JAVA_HOME/bin/java -jar sms2telegram.jar  