
package org.ajwcc.smslib.routing;

import java.util.Collection;
import org.ajwcc.smslib.gateway.AbstractGateway;
import org.ajwcc.smslib.message.OutboundMessage;

public abstract class AbstractBalancer
{
	public abstract Collection<AbstractGateway> balance(OutboundMessage message, Collection<AbstractGateway> candidates);
}
