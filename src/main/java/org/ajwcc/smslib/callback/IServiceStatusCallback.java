
package org.ajwcc.smslib.callback;

import org.ajwcc.smslib.callback.events.ServiceStatusCallbackEvent;

public interface IServiceStatusCallback
{
	public boolean process(ServiceStatusCallbackEvent event);
}
