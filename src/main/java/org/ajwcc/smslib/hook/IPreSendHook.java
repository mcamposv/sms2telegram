
package org.ajwcc.smslib.hook;

import org.ajwcc.smslib.message.OutboundMessage;

public interface IPreSendHook
{
	public boolean process(OutboundMessage message);
}
