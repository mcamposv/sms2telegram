/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.camposvarea.sms2telegram;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.Instant;
import org.ajwcc.pduUtils.gsm3040.*;

/**
 *
 * @author guybrush
 */
public class Sms {
    static Integer MAX_RETRY=15;
    Boolean debug=true;
    Long id;
    String rawsms;
    Integer retry_num;
    Long rcv_date;
    String status;
    String smsfrom;
    String smstxt;

    /**
     *
     * @param prawdata
     */
    public Sms(String prawdata){
        this((Long) null, prawdata, 0, Instant.now().getEpochSecond(), "NEW");
    }
    
    /**
     *
     * @param pid
     * @param prawsms
     * @param pretry_num
     * @param pretry_date
     * @param pstatus
     */
    public Sms(Long pid, String prawsms, Integer pretry_num, long prcv_date, String pstatus){
        id=pid;
        rawsms=prawsms;
        retry_num=pretry_num;
        rcv_date=prcv_date;
        status=pstatus;
        
        PduParser pdup = new PduParser();
        Pdu smspdu = pdup.parsePdu(prawsms);
        smsfrom=smspdu.getAddress();
        smstxt=smspdu.getAddress();
        
        StringBuilder sbuilder = new StringBuilder();
        if (smspdu.isConcatMessage()) sbuilder.append("C->");
        sbuilder.append(smspdu.getDecodedText());
        smstxt=sbuilder.toString();
    }
    
    
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("id:"); sb.append(id);
        sb.append(" rawsms:"); sb.append(rawsms);
        sb.append(" retry_num:"); sb.append(retry_num);
        sb.append(" rcv_date:"); sb.append(rcv_date);
        sb.append(" status:"); sb.append(status);
        sb.append(" smsfrom:"); sb.append(smsfrom);
        sb.append(" smstxt:"); sb.append(smstxt);

        return sb.toString();
        
    }
    
    

    public String getSmsfrom() {
        return smsfrom;
    }

    public String getSmstxt() {
        return smstxt;
    }

    public Long getId() {
        return id;
    }

    public String getRawsms() {
        return rawsms;
    }

    public Integer getRetry_num() {
        return retry_num;
    }

    public Long getrcv_date() {
        return rcv_date;
    }

    public String getStatus() {
        return status;
    }
    
    public void UpdateId(Long pid){
        id=pid;
    }

    public void sent(){
        status="OK";
        if (id != null) retry_num++;
    }
    public void fail(){
        if (retry_num.compareTo(MAX_RETRY) < 0) status="RT";
        else status="KO";
        retry_num++;
    }

}//sms.java
