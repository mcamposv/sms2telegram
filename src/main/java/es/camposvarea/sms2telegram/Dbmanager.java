/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.camposvarea.sms2telegram;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.sqlite.*;
import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 *
 * @author guybrush
 */
public class Dbmanager {
    private String dbfile;
    boolean debug=true;
    private String url ;
    Connection conn;
    static Integer dbversion = 1;
    
    
    public Dbmanager(String pdbfile){
        dbfile=pdbfile;
        if (debug) System.err.println("dbfile:"+pdbfile);
        url = "jdbc:sqlite:" + dbfile;
        Connect2Database();
    }
    
    private boolean Connect2Database(){
        try {
            conn = DriverManager.getConnection(url);
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                Integer dbversion_db = getDBVersion();
                if (dbversion_db == null) {
                    CreateVersionTable();
                    SetDBVersion();
                    CreateSMSTable();
                }
                else {
                    switch(dbversion_db.compareTo(dbversion)) {
                        case -1 :
                            System.out.println("Database Needs Upgrade");
                            break;
                        case 1 :
                            System.out.println("Database is for a Newer Version");
                            break;
                        default :
                            System.out.println("Database has the Right version");
                        }
                    }//there is db_version

            }
            return true;
         } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public Connection getConn() {
        return conn;
    }
    
    
    private boolean CreateVersionTable(){
                // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS versions (\n"
                + "    component text PRIMARY KEY,\n"
                + "    version integer NOT NULL\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            // create a new table
            stmt.execute(sql);
            return true;
        }
        catch(Exception e){
            System.out.println("CreateVersiontable: "+e);
            return false;
        }
    } 
    
    private boolean SetDBVersion(){
                // SQL statement for creating a new table
        String sql = "insert into versions\n"
                + " (component, version )\n"
                + " values (?,?);";
        String component ="DB_SCHEMA";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, component);
            pstmt.setInt(2, dbversion);
            pstmt.executeUpdate();
            return true;
        }
        catch(Exception e){
            System.out.println("insert DB_SCHEMA "+ e);
            return false;
        }
    } 
    
    private Integer getDBVersion(){
                // SQL statement for creating a new table
        String sql = "select version from versions where component=?;";
        String component ="DB_SCHEMA";
        Integer dbversion=null;
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, component);
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                dbversion=rs.getInt("version");
            }
            return dbversion;
        }
        catch(Exception e){
            System.out.println("Undetermined Version. "+ e);
            return null;
        }
    } 
        
        private boolean CreateSMSTable(){
                // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS sms (\n"
                + "    id integer primary key autoincrement,\n"
                + "    rawsms text NOT NULL,\n"
                + "    retry_num integer NOT NULL,\n"
                + "    rcv_date integer NOT NULL,\n"
                + "    status text NOT NULL,\n"
                + "    smsfrom text NOT NULL,\n"
                + "    smstxt text NOT NULL\n"
                + ");";
        String index = "CREATE INDEX sms_status ON sms(status);";
        String pk = "create index sms_pk on sms(id);";

        try {
            Statement stmt = conn.createStatement();
            // create a new table
            stmt.execute(sql);
            stmt.execute(pk);
            stmt.execute(index);
            return true;
        }
        catch(Exception e){
            System.out.println("CreateVersiontable: "+e);
            return false;
        }
    } 
        
    public void writetoSmS(Sms sms) throws Exception{

        if (sms.getId() == null){
            // is new record.
            String insert = "insert into sms\n"
                       + "(rawsms,retry_num,rcv_date,status,smsfrom,smstxt)\n"
                       + "values (?,?,?,?,?,?);";
            try {
                if (debug) System.out.println("Doing Insert");
                PreparedStatement pstmt = conn.prepareStatement(insert);
                if (debug) System.out.println("After Prepare Stament");
                pstmt.setString(1, sms.getRawsms());
                pstmt.setInt(2, sms.getRetry_num());
                pstmt.setLong(3, sms.getrcv_date());
                pstmt.setString(4, sms.getStatus());
                pstmt.setString(5, sms.getSmsfrom());
                pstmt.setString(6, sms.getSmstxt());
                if (debug) System.out.println("Before execute update");
                pstmt.executeUpdate();
                //TODO Verify the insert
                if (debug) System.out.println("Insert Done");
                if (debug) System.out.println("Getting generated Key");
                //get inserted ID
                ResultSet rs = pstmt.getGeneratedKeys();
                while (rs.next()) {
                    sms.UpdateId(rs.getLong("last_insert_rowid()"));
                    if (debug) System.out.println("Insertado id: "+ String.valueOf(sms.getId()));
                }
                    
            }
            catch(Exception e){
                System.out.println("insert DB_SCHEMA "+ e);
            }
        }
        else {            //needs to be update
            if (debug) System.out.println("Doing Update");
            String update = "update sms\n"
                       + "set retry_num=?,rcv_date=?,status=?\n"
                       + "where id=?;";
            try {
                if (debug) System.out.println("Doing update");
                PreparedStatement pstmt = conn.prepareStatement(update);
                if (debug) System.out.println("After Prepare Stament");
                pstmt.setInt(1, sms.getRetry_num());
                pstmt.setLong(2, sms.getrcv_date());
                pstmt.setString(3, sms.getStatus());
                pstmt.setLong(4, sms.getId());
                if (debug) System.out.println("Before execute update");
                pstmt.executeUpdate();
                if (debug) System.out.println("Update Done");
                //TODO check update code.
            }
            catch(Exception e){
                System.out.println("update DB_SCHEMA "+ e);
            }

        }//
    }

    public Connection getdbconnection(){
            return conn;
        }
    
    public List<Sms> getFailedSms(){
        Boolean debug=true;
        Long id;
        String rawsms;
        Integer retry_num;
        Long rcv_date;
        String status;
        List<Sms> lsms = new ArrayList<>();
        String sql = "select id,rawsms,retry_num,rcv_date,status from sms where status='RT';";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                id=rs.getLong("id");
                rawsms=rs.getString("rawsms");
                retry_num=rs.getInt("retry_num");
                rcv_date=rs.getLong("rcv_date");
                status=rs.getString("status");
                lsms.add(new Sms(id,rawsms,retry_num,rcv_date,status));
            }
            return lsms;
        }
        catch(Exception e){
            System.out.println("ERROR Getting FailedSmS: "+ e);
            return null;
        }
        
    }
}
