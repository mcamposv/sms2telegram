package es.camposvarea.utils;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;





/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author campom10
 */
public class Cfg {
    private final Logger logger = LogManager.getLogger(this.getClass().getName());
    private static final HashMap<String,String> hmcfg=new HashMap<String,String>();
    static final String CONFIGFILE= "cfg/sms2telegram.properties";
    static PropertiesConfiguration config;
    
    public Cfg() throws Exception {
       try{
        config = new Configurations().properties(CONFIGFILE);
        LoadCfgValues(config);
       }
       catch(Exception e) {
           logger.error("Failed to Loading Config File");
       }
    }// Constructor
/*
    public Cfg() throws Exception{
        logger.debug("CLASE: "+this.getClass().getName());
        
        logger.debug("Application diretory is:"+getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
               //TODO Read values from controlfile
        String appPath=getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        if (appPath.endsWith(".jar")){
          //I'm in the real world and running from a Jar file.
          appPath=appPath.substring(0,appPath.lastIndexOf("/")+1);
        }
        else if (appPath.endsWith("/build/classes/")){
          //I'm running from the development environment.
          appPath=appPath.substring(0,appPath.lastIndexOf("/build/classes/")+1);
        }
        logger.debug("My cfg file must be in:"+appPath+"cfg");
        File candidatefile= new File(appPath+"cfg/sms2telegram.cfg");
        if (candidatefile.exists() && candidatefile.canRead()){
            //Reading app file 
            cfgfile=candidatefile;
        }
        else {
            File etcfile= new File("/etc/sms2telegram.cfg");
            if (etcfile.exists() && etcfile.canRead()){
                //reading ETC
                cfgfile=candidatefile;
            }
            else {
                //Cant REad Config file
                throw (new Exception("Can't locate config file."));
            }
        }
        LoadCfgfile(cfgfile);
    }
*/
    public String GetCfgvalue(String key){
        return hmcfg.get(key);
    }
    
/*    private void LoadCfgfile(File file) throws Exception{
        try {
            FileInputStream fstream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
             //Read File Line By Line
            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
//                System.out.println (strLine);
                if (strLine.contains("=")) {
                    int position=strLine.indexOf("=");
                    String name=strLine.substring(0,position);
//                    System.out.println(name);
                    String value=strLine.substring(position+1,strLine.length());
//                    System.out.println(value);
                    if (name.equals("freq")||name.equals("dbretryinterval")) {
                        hmcfg.put(name, Long.valueOf(value)*1000L); //converting to milliseconds
                        }
                    else if (name.equals("dbpasswd")) {
                        Base64.Decoder decoder= Base64.getDecoder();
                        byte[] passwd = decoder.decode(value);
                        hmcfg.put(name,new String(passwd));
                        }
                    else {
                        hmcfg.put(name,value);
                        }
                    }
                }
            //Close the input stream
            br.close();   
//            System.out.println("Config Map"+hmcfg);
            }
        catch (Exception e) {
           logger.error("Error loading configfile");
           throw new Exception("Error loading configfile",e); 
        }
    }
*/
    private void LoadCfgValues(PropertiesConfiguration config) throws Exception{
        try {
            //we wil have to add all values we want / need to load.
            String[] keys = {"ModemDevice","ModemDriverClass","CheckInterval","SignalWeek","dbfile","TelegramRoomID","TelegramBootApiKey"};
            String key=null;
            for(int i=0; i< keys.length; i++){
                key=keys[i];
                hmcfg.put(key,config.getString(key));
            }
        }
        catch (Exception e) {
           logger.error("Error loading configfile");
           throw new Exception("Error loading configfile",e); 
        }
    }
}
