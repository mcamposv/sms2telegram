
package org.ajwcc.smslib.callback;

import org.ajwcc.smslib.callback.events.QueueThresholdCallbackEvent;

public interface IQueueThresholdCallback
{
	public boolean process(QueueThresholdCallbackEvent event);
}
