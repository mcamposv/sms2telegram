
package org.ajwcc.smslib.callback;

import org.ajwcc.smslib.callback.events.DeliveryReportCallbackEvent;

public interface IDeliveryReportCallback
{
	public boolean process(DeliveryReportCallbackEvent event);
}
