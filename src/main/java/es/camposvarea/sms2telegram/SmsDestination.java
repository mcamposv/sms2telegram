/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.camposvarea.sms2telegram;

/**
 *
 * @author guybrush
 * This Interface define basics for api to FS sms messages to other services
 */
public interface SmsDestination {
    public Boolean SendSMS(Sms psms);
    
}
