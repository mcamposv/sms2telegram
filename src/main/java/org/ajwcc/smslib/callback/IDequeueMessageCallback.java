
package org.ajwcc.smslib.callback;

import org.ajwcc.smslib.callback.events.DequeueMessageCallbackEvent;

public interface IDequeueMessageCallback
{
	public boolean process(DequeueMessageCallbackEvent event);
}
