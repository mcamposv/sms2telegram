
package org.ajwcc.smslib.callback;

import org.ajwcc.smslib.callback.events.GatewayStatusCallbackEvent;

public interface IGatewayStatusCallback
{
	public boolean process(GatewayStatusCallbackEvent event);
}
