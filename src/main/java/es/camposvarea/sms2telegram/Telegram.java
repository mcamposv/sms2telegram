package es.camposvarea.sms2telegram;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 *
 * @author guybrush
 */
public class Telegram implements SmsDestination{
    String chatID;
    String botApiKey;

    public Telegram(String pchatID, String pbotApiKey){
        chatID=pchatID;
        botApiKey=pbotApiKey;
    }

    @Override
    public Boolean SendSMS(Sms psms) {
        try {
        String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
        
        StringBuilder sbm = new StringBuilder();
        sbm.append("SmS From: ");
        sbm.append(psms.getSmsfrom()+"\n");
        sbm.append("Text: ");
        sbm.append(psms.getSmstxt());
        

        urlString = String.format(urlString, botApiKey, chatID, URLEncoder.encode(sbm.toString(),"UTF-8"));

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        StringBuilder sb = new StringBuilder()  ;
        InputStream is = new BufferedInputStream(conn.getInputStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String inputLine = "";
        while ((inputLine = br.readLine()) != null) {
            sb.append(inputLine);
        }
        String response = sb.toString();
            System.out.println("Respuesta: "+ response);
        return true; //mesage sent.
        }
        catch (Exception e) {
            System.out.println("SendSms:"+e);
            return false;
        }
    }
    
}
