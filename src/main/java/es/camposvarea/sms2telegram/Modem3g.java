package es.camposvarea.sms2telegram;
import com.fazecast.jSerialComm.SerialPort;
import com.google.common.primitives.Bytes;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.lang.StringBuilder;
import org.apache.commons.codec.binary.Hex;
import org.ajwcc.pduUtils.gsm3040.*;

/**
 *
 * @author guybrush
 */

public abstract class Modem3g {
        SerialPort modem;
        InputStream Input;
        OutputStream Output;
        boolean debug=false;
//        boolean debug=true;
    
    public Modem3g(String port) throws Exception {
     try {
        modem = SerialPort.getCommPort(port);
        if ( modem != null ) {
            if (debug)  System.out.println("Port Exist:"+modem.getSystemPortName());
            modem.setComPortParameters(115200, 8, 1, 0);
            modem.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 2000, 2000);
            modem.openPort();
            if( ! modem.isOpen()) throw new Exception("I could not Open the port");
            Input = modem.getInputStream();
            if (Input != null ) if (debug) System.out.println("The Input Stream Was Configured");
            Output = modem.getOutputStream();
            if (Output != null ) if (debug) System.out.println("The Output Stream Was Configured");
        }
        check();
     }
     catch (Exception e) {
         System.out.println("Something went wrong.");
         throw new Exception("Error constructing modem");
     }
        
    }
    public void cleanInput(){
        try {
            if (Input.available() != 0) {
                System.out.println("Cleaning Pending data"+String.valueOf(Input.available()));
                byte[] Buffer = new byte[Input.available()];
                int amount = Input.read(Buffer, 0, Buffer.length);
                String Sbuffer = new String(Buffer,"UTF-8");
                System.out.println("Mensaje: " + Sbuffer);
            }
        }
            catch (Exception e){
                    System.out.println("puff");
                    }
        
    }
    
    private byte [] cleanAnswer(byte [] answer){
            byte[] separator = {0x0D,0x0D,0x0A};
            int possition = Bytes.indexOf(answer, separator);
//            System.out.println("DEBUG: Posicion del comando es:"+String.valueOf(possition));
            byte[] cleanAnswer = Arrays.copyOfRange(answer, possition+(separator.length), answer.length);

                while (hasLineFeedEnd(cleanAnswer)) {
                    cleanAnswer=Arrays.copyOfRange(cleanAnswer,0, cleanAnswer.length-2);
                }
            return cleanAnswer;
    }
    
    private byte [] cleanAnswerWithOK(byte [] answer){
            byte[] separator = {0x0D,0x0D,0x0A};
            int possition = Bytes.indexOf(answer, separator);
//            System.out.println("DEBUG: Posicion del comando es:"+String.valueOf(possition));
            byte[] cleanAnswer = Arrays.copyOfRange(answer, possition+(separator.length), answer.length);
                while (hasLineFeedEnd(cleanAnswer)||isAnswerOK(cleanAnswer) ) {
                    cleanAnswer=Arrays.copyOfRange(cleanAnswer,0, cleanAnswer.length-2);
                }
                return cleanAnswer;
    }
    
    private boolean hasLineFeedEnd(byte [] answer) {
        if (answer.length>1) {
            byte[] retorno= {0x0D,0x0A};
            if (Arrays.equals( retorno, Arrays.copyOfRange(answer, answer.length-2, answer.length))) {
                return true;
            }
        return false;    
        }
        else return false;           
    }
    
    
    private boolean isAnswerOK(byte [] panswer) {
            try{
                byte [] answer = panswer;
                while (hasLineFeedEnd(answer)) {
                    answer=Arrays.copyOfRange(answer,answer.length-2, answer.length);
                }
                byte[] OK = Arrays.copyOfRange(answer,answer.length-2, answer.length);
                String Sbuffer = new String(OK,"UTF-8");
                if ("OK".matches(Sbuffer)) {
                    return true;
                }
                else  return false;
            }
            catch(Exception e) {
                return false;
            }
    }
        private boolean isAnswerOKWithEnding(byte [] answer) {
            try{
                byte[] OK = Arrays.copyOfRange(answer,answer.length-4, answer.length-2);
                String Sbuffer = new String(OK,"UTF-8");
                if ("OK".matches(Sbuffer)) {
                    return true;
                }
                else  return false;
            }
            catch(Exception e) {
                return false;
            }
 }
    public boolean check (){
        try {
            cleanInput();
            byte[] command ="AT\r\n".getBytes();
            //separador comado y respuesta ODOD0A
            byte[] separator = {0x0D,0x0D,0x0A};
            Output.write(command);
            Output.flush();
//            if ( Input.available() == 0) Input.wait(1000);
            if ( Input.available() == 0) Thread.sleep(1000);
            if ( Input.available() == 0) return false;
            else {
                   byte[] Buffer = new byte[Input.available()];
                   int amount = Input.read(Buffer, 0, Buffer.length);
                   String Sbuffer = new String(cleanAnswer(Buffer),"UTF-8");
                   if (this.isAnswerOKWithEnding(Buffer)) { 
                       if (debug)  System.out.println("The check was OK");
                       return true;
                        }
                   else {
                        System.out.println("ERROR: La Respuesta fue:" + Sbuffer);
                        if (debug) System.out.println("DEBUG: FullAnswer:" + Hex.encodeHexString(Buffer,false) );
                        if (debug) System.out.println("DEBUG: Command:   " + Hex.encodeHexString(command,false) );
                        if (debug) System.out.println("DEBUG: Answer :   " + Hex.encodeHexString(Sbuffer.getBytes(),false) );
                        return false;       
                        }
            }
        }
        catch (Exception e) {
            System.out.println("Vaya Ostia");
            e.printStackTrace();
            modem.closePort();
        }
            return false;
    }

        public String ExecuteCommand (String pcommand){
        try {
            cleanInput();
            byte[] command =(pcommand+"\r\n").getBytes();
            //separador comado y respuesta ODOD0A
            byte[] separator = {0x0D,0x0D,0x0A};
            Output.write(command);
            Output.flush();
//            if ( Input.available() == 0) Input.wait(1000);
            if ( Input.available() == 0) Thread.sleep(1000);
            if ( Input.available() == 0) return null;
            else {
                   byte[] Buffer = new byte[Input.available()];
                   int amount = Input.read(Buffer, 0, Buffer.length);
                   if (debug) System.out.println("DEBUG: Answer Before Clean:   " + Hex.encodeHexString(Buffer,false) );
                   String Sbuffer = new String(cleanAnswerWithOK(Buffer),"UTF-8");
                   if (isAnswerOKWithEnding(Buffer)) {
                       if (debug) System.out.println("DEBUG: Answer :   " + Hex.encodeHexString(Sbuffer.getBytes(),false) );
                       if (debug) System.out.println("DEBUG: strAnswer :   " + Sbuffer);
                       return Sbuffer;
                        }
                   else {
                        System.out.println("ERROR: La Respuesta fue:" + Sbuffer);
                        if (debug) System.out.println("DEBUG: FullAnswer:" + Hex.encodeHexString(Buffer,false) );
                        if (debug) System.out.println("DEBUG: Command:   " + Hex.encodeHexString(command,false) );
                        if (debug) System.out.println("DEBUG: Answer :   " + Hex.encodeHexString(Sbuffer.getBytes(),false) );
                        return null;       
                        }
            }
        }
        catch (Exception e) {
            System.out.println("Vaya Ostia");
            e.printStackTrace();
            modem.closePort();
        }
            return null;
    }
    public boolean smsInMem() throws Exception{
        int nmensajes = this.nsmsInMem("SM");
        if ( nmensajes > 0 ) return true;
        else return false;
    }
    public int nsmsInMem(String memname) throws Exception {
        String answer = ExecuteCommand("AT+CPMS?");
        int ini = answer.indexOf("\"");
        if (ini >= 0) {
            answer=answer.substring(ini);
            if (debug) System.out.println("valida Answer data:"+answer);
            //iterate over the array.
            String [] aanswer = answer.split(",");
            int index=0;
            while (index+2 <= aanswer.length -1)
            {
                if (debug) System.out.println("procesing: "+aanswer[index]);
                if (aanswer[index].contains(memname)){
                   if (debug) System.out.println("found Memory info");
                   return Integer.valueOf(aanswer[index+1]);
                }
                index++;
            }
            throw new Exception("Can not find the memory info for:"+ memname);
        }
        return  Integer.valueOf(answer.split(",")[1]).intValue();
    }
    public int nsmsInMem() throws Exception{
        String answer = ExecuteCommand("AT+CPMS?");
//        System.out.println(answer);
        return this.nsmsInMem("SM");
    }
    public String getAllsms(){
        String answer = ExecuteCommand("AT+CMGL=4");
        return answer;
    }
        
    public List<Integer> getsmspossitions(){
        List<Integer> poss = new LinkedList<Integer>();
        List<String> smsdata = new LinkedList<String>(Arrays.asList(getAllsms().split("\n")));
        if (debug) System.out.println("DEBUG: Size del array Lines: "+ String.valueOf(smsdata.size()));
        //Remove message data.
        Iterator<String> ITallSms = smsdata.iterator();
        if (debug) System.out.println("DEBUG: Voy al while");
        while ( ITallSms.hasNext()) {
            String line= ITallSms.next();
            if (debug) System.out.println("DEBUG: Procesando: "+line);
           if( !line.contains("+CMGL") ) {
                //Remove text entry
                if (debug) System.out.println("DEBUG: Antes de remover");
                ITallSms.remove();
                if (debug) System.out.println("DEBUG: Despues de remover");
           }
        }
        //Convert mesage metadata into position list
        if (debug) System.out.println("DEBUG: New Size: " + String.valueOf(smsdata.size()));
        Iterator<String> ITsmsmdata = smsdata.iterator();
        if (debug) System.out.println("DEBUG: Tras crear segundo Iterator");
        while ( ITsmsmdata.hasNext()) {
            String sms= ITsmsmdata.next();
            if (debug) System.out.println("posicion:"+(sms.split(":")[1].split(",")[0]));
            poss.add(Integer.valueOf((sms.split(":")[1].split(",")[0]).trim()));
            if (debug) System.out.println("DEBUG: Despues de añadir");
        }
        
        return poss;
    }
    
    public String extractsmsdata(String rawsmsdata) {
        List<String> smsdata = new LinkedList<String>(Arrays.asList(rawsmsdata.split("\n")));
        Iterator<String> ITallSms = smsdata.iterator();
        if (debug) System.out.println("DEBUG: Nlines Before: "+ String.valueOf(smsdata.size()));
        while ( ITallSms.hasNext()) {
            String line= ITallSms.next();
            if (debug) System.out.println("DEBUG: Procesando: "+line);
           if( line.contains("+CMGR") ) {
                //Remove text entry
                if (debug) System.out.println("DEBUG: Antes de remover");
                ITallSms.remove();//removing the iterator removes from original list.
                if (debug) System.out.println("DEBUG: Despues de remover");
           }
        }
        StringBuilder data = new StringBuilder();
        if (debug) System.out.println("DEBUG: Nlines After: "+ String.valueOf(smsdata.size()));
        for (int i=0;i<smsdata.size();i++){
            data.append(smsdata.get(i));
        }
        return data.toString();
    }
    public String getsmsraw(Integer possition) throws Exception{
        String answer=ExecuteCommand("AT+CMGR="+String.valueOf(possition));//(Separador info from bytes. 0D0A)
        if (debug) System.out.println("DEBUG: Leyedo:" + answer);
        String smsdata=extractsmsdata(answer);
        if (debug) System.out.println("DEBUG: RawData:" + smsdata);
        
        return smsdata;
//        return (Arrays.toString(Hex.decodeHex(smsdata)));
//        return Arrays.toString(Hex.encodeHex(smsdata.getBytes(),false));
    }

    public String getsms(Integer possition) throws Exception{
        String Rawdata = getsmsraw(possition);
        PduParser pdup = new PduParser();
        Pdu smspdu = pdup.parsePdu(Rawdata);
        StringBuilder sbuilder = new StringBuilder();
        if (smspdu.isConcatMessage()) sbuilder.append("Continue\n");
        sbuilder.append("From: ").append(smspdu.getAddress());
        sbuilder.append("\nText: ").append(smspdu.getDecodedText());
        return sbuilder.toString();
//        return (Arrays.toString(Hex.decodeHex(smsdata)));
//        return Arrays.toString(Hex.encodeHex(smsdata.getBytes(),false));
    }

    public void deleteAllSms(Integer pos){
        String answer = ExecuteCommand("AT+CMGD=\"all\"");
    }
    
    public void deleteSms(Integer pos){
        String answer = ExecuteCommand("AT+CMGD="+pos);
    }

}