
package org.ajwcc.smslib.callback;

import org.ajwcc.smslib.callback.events.MessageSentCallbackEvent;

public interface IMessageSentCallback
{
	public boolean process(MessageSentCallbackEvent event);
}
