
package org.ajwcc.smslib.callback;

import org.ajwcc.smslib.callback.events.InboundMessageCallbackEvent;

public interface IInboundMessageCallback
{
	public boolean process(InboundMessageCallbackEvent event);
}
